config
CurrentUser
CurrentUserHasRight
DisallowExecuteCode
DisplayName
EquiveObjects
GetPrincipalTypeForACL
GrantRight
HasRight
HasRights
IncludeSuperusers
IncludeSystemRights
InvalidateACLCache
IsGroup
IsRoleGroup
IsUser
params
PrincipalObj
PrincipalType
ReferenceId
reinitializes
RevokeRight
RightName
RolesWithRight
RT
SetDisabled
SetPrincipalType
smallint
SuperUser
undef
varchar
