# Lintian overrides for request-tracker5

# Allowing world read access on these directories allows bypassing of
# application defined permissions.

non-standard-dir-perm 2755 != 0755 [var/log/request-tracker5/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/mason_data/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/mason_data/etc/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/mason_data/obj/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/mason_data/cache/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker5/session_data/]
non-standard-dir-perm 2750 != 0755 [var/lib/request-tracker5/]
non-standard-dir-perm 2750 != 0755 [var/lib/request-tracker5/data/]
non-standard-dir-perm 0700 != 0755 [var/lib/request-tracker5/data/gpg/]

# This file is part of a customised version of ckeditor.
embedded-javascript-library please use ckeditor [usr/share/request-tracker5/static/RichText/ckeditor.min.js]
embedded-javascript-library please use libjs-jquery [usr/share/request-tracker5/static/RichText/adapters/jquery.js]

# TableSorter 2.0 not available in Debian.
embedded-javascript-library please use libjs-jquery-tablesorter [usr/share/request-tracker5/static/js/jquery.tablesorter.min.js]

# jQuery Cookie Plugin 1.3.1 not available in Debian.
embedded-javascript-library please use libjs-jquery-cookie [usr/share/request-tracker5/static/js/jquery.cookie.js]

# jQuery 1.13.0 not available in Debian.
embedded-javascript-library please use libjs-jquery-ui [usr/share/request-tracker5/static/js/jquery-ui.min.js]
embedded-javascript-library please use libjs-jquery-ui [usr/share/request-tracker5/static/css/elevator-light/jquery-ui.css]

# jQuery 3.6.0 is in Debian, but...
embedded-javascript-library please use libjs-jquery [usr/share/request-tracker5/static/js/jquery-3.6.0.min.js]

# We give full paths in the generated cron file.
command-with-path-in-maintainer-script /usr/sbin/rt-clean-attributes-5 (in test syntax) [postinst:148]
command-with-path-in-maintainer-script /usr/sbin/rt-clean-attributes-5 (plain script) [postinst:148]
command-with-path-in-maintainer-script /usr/sbin/rt-clean-shorteners-5 (in test syntax) [postinst:149]
command-with-path-in-maintainer-script /usr/sbin/rt-clean-shorteners-5 (plain script) [postinst:149]
command-with-path-in-maintainer-script /usr/sbin/rt-email-dashboards-5 (in test syntax) [postinst:147]
command-with-path-in-maintainer-script /usr/sbin/rt-email-dashboards-5 (plain script) [postinst:147]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-5 (in test syntax) [postinst:145]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-5 (plain script) [postinst:145]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-5 (in test syntax) [postinst:146]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-5 (plain script) [postinst:146]

# These aren't documentation files.
package-contains-documentation-outside-usr-share-doc [usr/share/request-tracker5/html/*
package-contains-documentation-outside-usr-share-doc [usr/share/request-tracker5/static/RichText/*

# Mason has multiple directories, including cache.
repeated-path-segment cache [var/cache/request-tracker5/mason_data/cache/]

# The debconf templates are accessed in a for loop which is why lintian can't
# find them.
unused-debconf-template request-tracker5/commentaddress*
unused-debconf-template request-tracker5/correspondaddress*
unused-debconf-template request-tracker5/organization*
unused-debconf-template request-tracker5/rtname*
unused-debconf-template request-tracker5/webbaseurl*
unused-debconf-template request-tracker5/webpath*

# Documentation doesn't make sense for a dummy service.
systemd-service-file-missing-documentation-key [usr/lib/systemd/system/request-tracker5.service]

# Status doesn't make sense for a dummy init script.
init.d-script-does-not-implement-status-option [etc/init.d/request-tracker5]

# These packages don't provide a version of LGPL
copyright-refers-to-symlink-license usr/share/common-licenses/LGPL

# We need a fonts-inter-web package what includes the woff/woff2 fonts.
font-outside-font-dir [usr/share/request-tracker5/static/css/fonts/inter/*
font-in-non-font-package [usr/share/request-tracker5/static/css/fonts/inter/*
