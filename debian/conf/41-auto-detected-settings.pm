# Auto enable features if optional modules are installed. You can disable
# these in a file which sorts after this one if you wish.

# If libcss-inliner-perl is installed, enable it.
if (-f '/usr/share/perl5/CSS/Inliner.pm') {
    Set($EmailDashboardInlineCSS, 1);
}
