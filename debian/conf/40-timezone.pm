# dynamically find out the current timezone
my $zone = "UTC";

my $etc_localtime_symlink = readlink("/etc/localtime");
if ( $etc_localtime_symlink && $etc_localtime_symlink =~ /^(?:..)?\/usr\/share\/zoneinfo\/(.*)$/ ) {
  $zone=$1;
} elsif ( -f "/etc/timezone" ) {
  $zone=`/bin/cat /etc/timezone`;
  chomp $zone;
}

Set($Timezone, $zone);
