request-tracker5 for Debian
---------------------------

Installation
------------
Read NOTES.Debian for installation and configuration notes.

I mean this.

Yes, you. You've installed the package, right? You need to configure
it before using it. NOTES.Debian is bursting with config-goodness.

Seriously. You can't install a complex package like this and expect it
to just work.

RT_SiteConfig.pm permissions
----------------------------

If you reject the "Handle RT_SiteConfig.pm permissions" debconf question,
the file is only readable by root after installation because it is going 
to contain the database password. You will need to make it readable yourself
to the www-data user or equivalent when setting up the web server, but be 
sure to consider the security implications (like local PHP scripts that run 
as the same user.)

standalone_httpd
----------------

RT5 comes with a script to allow it to be run in a standalone mode
that does not require apache to be installed, known as standalone_httpd.
This is only suitable for development and testing purposes and is only
installed as an example in this package, because it requires packages
not available in Debian.

Upgrading from request-tracker3.8 to request-tracker4 or 5
----------------------------------------------------------

The MOST IMPORTANT thing to note is that if you have more than one of
the request-tracker... packages installed at the same time then you
will have to be careful to use the correct version of commands such as
rt-setup-database, rt-mailgate and rt-crontool, etc.. If in any doubt, 
add on the -3.6, -3.8, -4, -5 [...] extension to the name of the command. 
The commands are all managed via the update-alternatives script.

If you are considering upgrading your system and want to keep your
current data we strongly recommend doing a dump of your current
database and then restoring into a new separate database, for use by
RT 5. This will allow you to attempt the upgrade and changes required
without destroying your existing data. It will also allow you to run
the various versions of RT side-by-side without any chance of  conflicts.

You will need to specify the correct rt username and password for your
installation. If you are using PostgreSQL, as with the original
install, you will probably need to allow access to the database
template1 for your rtuser, see NOTES.Debian for details.

In addition to the notes here, please ensure that you have read README
and the various UPGRADING files, (including, if using MySQL,
UPGRADING.mysql), for more important notes about moving between versions;
in particular, the database schema and content updates. The 'etc' directory
mentioned in those documents is available at /usr/share/request-tracker5/etc
on Debian systems.

Once you've configured RT to use your existing RT 3.8 or 4 database, you
should use upgrade functionality of rt-setup-database (running as the user
the web server runs as, usually www-data)

Whilst most data is not automaticalky migrated, note that a new
installation of request-tracker5 will copy GPG data from system's RT4
installation, if present. If you continue to run both systems in parallel,
you may need to manually synchronise the state of this data.

Upgrading the request-tracker5 package
--------------------------------------
There are often notes in NEWS.Debian that describe schema changes you
should make to your database, often for performance reasons.


Config changes
--------------

If you change /etc/request-tracker5/RT_SiteConfig.pm and use
Apache's modperl, you have to re-start Apache: '/etc/init.d/apache2
restart'. Don't use the 'apache2ctl graceful' or 'apache2ctl restart';
they're not quite enough.

Upgrading from RT2
------------------

The tools to do this are available at:

http://search.cpan.org/dist/RT-Extension-RT2toRT3/

This really is not the easiest thing in the world but is achievable
with a bit of thought and careful planning. The way to go about it is
to dump the RT2 database using the tool provided (rt-2.0-to-dumpfile)
whilst you still have RT2 installed. Once that is done, and it might
take sometime - we are talking days here for a big database - install
the RT3 Debian package. Go through all the notes in the NOTES.Debian
file then load the old data with the tool provided
(dumpfile-to-rt-3.0) into your nice shiny, newly prepared, RT3
database - again this can take days for a big database.

If you are reading this then you have probably already removed your
RT2 Debian package and installed RT3 instead, your best plan is to
download a tarball of RT2. Unpack it somewhere like
/usr/local/share/rt2/ and then edit the rt2-to-rt3 dump script to
point to the correct place. The lines to change are something like:

use lib "/opt/rt-bps/lib";
use lib "/opt/rt-bps/etc";

replacing the /opt/rt-bps/lib with the path to your RT2 libraries and
/opt/rt-bps/etc with the path to your RT2 configuration files.

Note: the rt2-to-rt3 extension may not yet updated to fully support
the current version of RT. It may be necessary to perform a staged
upgrade via an earlier version.

Any problems/questions about upgrading from RT2 to RT3 should be sent
to the rt-users mailing list at rt-users@lists.bestpractical.com


Customising RT
--------------

A comprehensive guide to customising RT is available online at
<http://requesttracker.wikia.com/wiki/Customizing>. This includes
descriptions of the preferred modification techniques such as
callbacks and extensions (installed into
/usr/local/share/request-tracker5/plugins on Debian systems).

However, you can also make local changes to the RT web interface, by
copying the relevant mason or html files into the
/usr/local/share/request-tracker5/html hierarchy, preserving the
directory structure. Any changes you make will immediately have an
effect on the web interface, you do not need to restart the web
server. You should ensure that the correct permissions are set on the
files so that the webserver can access them.

If you want to make local changes to the RT library code, you can copy
the relevant Perl libraries into the
/usr/local/share/request-tracker5/lib hierarchy, preserving the
directory structure.

Note that local additions and changes should ALWAYS be put into the
/usr/local directories. Any changes to files that are shipped with
request-tracker5 will be lost at the next update unless they are
conffiles (i.e. in /etc/request-tracker5). Any files you add into the
/usr/local/share/request-tracker5 hierarchy will be preserved if you
decide to remove or purge the package.

If you need any additional modules to be loaded at start time, you
can put "use MyMod::Foo;" lines into
/etc/request-tracker5/RT_SiteModules.pm. This is a Debian modification.

/usr/local is read-only
-----------------------

Sometimes systems have a /usr/local/ that is mounted read-only so you
cannot make use of the custom hierarchy of request-tracker5 that would
otherwise be there. In this case you can alter the place where the
custom hierarchy is found by overlaying the RT.pm with an RT_Local.pm in
/usr/share/perl5/ which would look something like this:

##########
$LocalPath = '/path/to/somewhere/else/request-tracker5';
$LocalLexiconPath = $LocalPath . '/po';

$MasonLocalComponentRoot = $LocalPath . '/html';

unshift @INC, ($LocalPath . '/lib') ;

1;
##########

Remember to restart the webserver after creating the overlay file if
you are using mod_perl.
 
Authors
-------

Stephen Quinney <sjq@debian.org>, Thu Sep 15 14:57:05 2005
Niko Tyni <ntyni@iki.fi> Mon, 21 Aug 2006 00:05:45 +0300
Dominic Hargreaves <dom@earth.li>, Sun Jan 30 13:17:49 UTC 2011
