A separate bundle containing the uncompressed source to some bundled
javascript is provided by upstream and is part of this source package.
It will be unpacked as third-party-source/ by dpkg-source -x.

This package is maintained using git-dpm. Version >= 0.50 is needed,
as we make use of the Patch-Name feature.

Because of some interesting behaviour relating to components[1], new
upstream imports need to be done in a similar way to this:

git-dpm import-tar -p upstream ../request-tracker5_5.0.7+dfsg.orig.tar.gz
git log|head -1|cut -f2 -d\    # Note: there is a space after the \
[note down the commit]
git checkout master
### See notes below about repacking the third party source tarball.
git-dpm import-new-upstream --ptc \
  --component ../request-tracker5_5.0.7+dfsg.orig-third-party-source.tar.gz \
  -p [commit from above] ../request-tracker5_5.0.7+dfsg.orig.tar.gz
git-dpm rebase-patched
# Perform any fixes or removals as required.
git-dpm update-patches

[1] <http://lists.alioth.debian.org/pipermail/pkg-request-tracker-maintainers/2013-March/003502.html>

Note for RT5:

For the 5.0.x release, some unminified sources were missing from the
upstream third-party-source tarball. The Debian maintainers
have repacked the third-party tarball with the following changes:

* Imported ckeditor source from the canonical upstream locations
  (see debian/fixup-third-party.sh for details)

In addition, we have updated the build rules to build the final ckeditor
  files from this source (see debian/build-final-ckeditor.sh)

We are working with upstream to improve the process of maintaining
the third-party tarball for future releases. See
<https://rt.bestpractical.com/Ticket/Display.html?id=37009>.

Example use of debian/maint/fixup-third-party.sh

tar zxvf rt-5.0.7-third-party-source.tar.gz
cd request-tracker5/debian/maint
./fixup-third-party.sh ../../../ckeditor4/ ../../../BootstrapCK4-Skin/ \
    ../../../confighelper/ ../../../ccmsconfighelper ../../../docfont 4.20.1
cd ../../..
tar zcvf request-tracker5_5.0.7+dfsg.orig-third-party-source.tar.gz \
    rt-5.0.7
cp -p rt-5.0.7.tar.gz \
    request-tracker5_5.0.7+dfsg.orig.tar.gz
