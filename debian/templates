# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: request-tracker5/rtname
Type: string
_Description: Name for this Request Tracker (RT) instance:
 Every installation of Request Tracker must have a unique name.
 The domain name or an abbreviation of the organization name are
 usually good candidates.
 .
 Please note that once you start using a name, you should probably never
 change it. Otherwise, mail for existing tickets won't get put in the right
 place.
 .
 This setting corresponds to the $rtname configuration variable.

Template: request-tracker5/organization
Type: string
_Description: Identifier for this RT instance:
 In addition to its name, every installation of Request Tracker must also have
 a unique identifier. It is used when linking between RT installations.
 .
 It should be a persistent DNS domain relating to your installation, for
 example example.org, or perhaps rt.example.org. It should not be changed
 during the lifetime of the RT database, so it is recommended not to use the
 default value of the system hostname. Therefore, if you plan to restore
 an existing database to this installation, you should use the same value
 as previous installations using the same database.
 .
 This setting corresponds to the $Organization configuration variable.

Template: request-tracker5/correspondaddress
Type: string
_Description: Default email address for RT correspondence:
 Please choose the address that will be listed in From: and Reply-To: headers of
 emails tracked by RT, unless overridden by a queue-specific
 address.
 .
 This setting corresponds to the $CorrespondAddress configuration variable.

Template: request-tracker5/commentaddress
Type: string
_Description: Default email address for RT comments:
 Please choose the address that will be listed in From: and Reply-To: headers of comment
 emails, unless overridden by a queue-specific address. Comments can be
 used for adding ticket information that is not visible to the client.
 .
 This setting corresponds to the $CommentAddress configuration variable.

Template: request-tracker5/webbaseurl
Type: string
_Description: Base URL for the RT web interface:
 Please specify the scheme, server and (optionally) port for constructing
 RT web interface URLs.
 .
 The value should not have a trailing slash (/).
 .
 This setting corresponds to the $WebBaseURL configuration variable.

Template: request-tracker5/webpath
Type: string
_Description: Path to the RT web interface:
 If the RT web interface is going to be installed somewhere other than at
 the documents root of the web server, you should specify the path to it here.
 .
 The value requires a leading slash (/) but not a trailing one.
 .
 This setting corresponds to the $WebPath configuration variable.

Template: request-tracker5/handle-siteconfig-permissions
Type: boolean
_Description: Handle RT_SiteConfig.pm permissions?
 The RT web interface needs access to the database password, stored in the
 main RT configuration file. Because of this, the file is made readable by
 the www-data group in normal setups. This may have security implications.
 .
 If you reject this option, the file will be readable only by root, and
 you will have to set up appropriate access controls yourself.
 .
 With the SQLite backend, this choice will also affect the
 permissions of automatically-generated local database files.

Template: request-tracker5/install-cronjobs
Type: boolean
_Description: Install cron jobs?
 Some features of RT depend on cron jobs, and they can be set up for you
 by this package. You should normally accept this option unless you are
 working on a snapshot of data (and would like to avoid events which send
 out email to users) or this system will be part of a cluster (in which
 case only one system should have cron jobs enabled).

Template: request-tracker5/initial-root-password
Type: password
_Description: Initial root password for RT system:
 The RT system will be populated with an initial superuser, named 'root',
 and the password you provide here will be used as the initial password
 of this superuser. It should be five characters or more.
