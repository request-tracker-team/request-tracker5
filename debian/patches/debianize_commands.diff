From aba8f50901287a37880b7ae05712926a3d850d92 Mon Sep 17 00:00:00 2001
From: Andrew Ruthven <andrew@etc.gen.nz>
Date: Thu, 9 Jul 2020 22:53:09 +1200
Subject: Use Debian location of commands and data

Forwarded: not-needed
Patch-Name: debianize_commands.diff
---
 docs/authentication.pod                       |  2 +-
 docs/automating_rt.pod                        | 16 +++++-----
 docs/customizing/assets/tutorial.pod          |  6 ++--
 .../scrip_conditions_and_action.pod           |  6 ++--
 docs/customizing/search_result_columns.pod    |  8 ++---
 docs/extending/external_custom_fields.pod     |  8 ++---
 docs/extensions.pod                           |  7 ++---
 docs/full_text_indexing.pod                   | 30 +++++++++----------
 docs/incremental-export/README                | 15 ++++++----
 docs/initialdata.pod                          |  2 +-
 docs/reminders.pod                            |  2 +-
 docs/system_administration/database.pod       | 10 +++----
 docs/tracking-rt-configuration.pod            | 10 +++----
 13 files changed, 62 insertions(+), 60 deletions(-)

diff --git a/docs/authentication.pod b/docs/authentication.pod
index bfbf5f7e..0ed638b3 100644
--- a/docs/authentication.pod
+++ b/docs/authentication.pod
@@ -119,7 +119,7 @@ An example of using LDAP authentication and HTTP Basic auth:
 All of the following options control the behavior of RT's built-in external
 authentication which relies on the web server.  They are documented in detail
 under the "Authorization and user configuration" section of L<RT_Config>
-and you can read the documentation by running C<perldoc /opt/rt5/etc/RT_Config.pm>.
+and you can read the documentation by running C<perldoc /usr/share/request-tracker5/etc/RT_Config.pm>.
 
 The list below is meant to make you aware of what's available.  You should read
 the full documentation as described above.
diff --git a/docs/automating_rt.pod b/docs/automating_rt.pod
index c3697e8e..3af0f69b 100644
--- a/docs/automating_rt.pod
+++ b/docs/automating_rt.pod
@@ -15,7 +15,7 @@ different ways you can automate tasks.
 
 All of the options for L<rt-crontool> are documented with the tool itself:
 
-    $ perldoc /opt/rt5/bin/rt-crontool
+    $ perldoc /usr/bin/rt-crontool-5
 
 and on the Best Practical web site.
 
@@ -62,7 +62,7 @@ Starting with a simple example, this command performs a search and
 displays output, but doesn't do anything to the returned tickets.
 This can be useful for safely testing search criteria.
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "Owner = 'root'" \
         --action RT::Action \
         --verbose \
@@ -99,7 +99,7 @@ You can auto-set status based on any criteria you can define in
 a TicketSQL statement. For example, this command will resolve all
 active tickets that haven't been acted on in a month or more:
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "(Status != 'resolved' AND Status != 'rejected') \
                        AND LastUpdated <= '1 month ago'" \
         --action RT::Action::SetStatus \
@@ -129,7 +129,7 @@ the selected tickets, and the transition must be allowed.
 The following command records a comment on all tickets returned from the
 query -- in this case, tickets that are new and unowned after 3 days.
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "Owner = 'Nobody' AND Status = 'new' \
                       AND Created < '3 days ago'" \
         --action RT::Action::RecordComment \
@@ -153,7 +153,7 @@ using the L<RT::Action::RecordCorrespondence> module.
 While the example above sends notifications as a side-effect of recording
 a comment, you can also send notifications directly.
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "(Status != 'resolved' AND Status != 'rejected') \
                       AND Queue = 'Project Work'" \
         --condition RT::Condition::Overdue \
@@ -193,7 +193,7 @@ date and are still not resolved have priority escalated automatically.
 
 This command escalates tickets in a designated queue:
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::ActiveTicketsInQueue \
+    /usr/bin/rt-crontool-5 --search RT::Search::ActiveTicketsInQueue \
         --search-arg "General" \
         --action RT::Action::EscalatePriority
 
@@ -201,7 +201,7 @@ The C<--search-arg> is the name of the queue in which to escalate tickets.
 As shown in previous examples, you can also set your criteria using a
 TicketSQL query as well:
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "(Status='new' OR Status='open') AND Due > 'Jan 1, 1970'" \
         --action RT::Action::EscalatePriority
 
@@ -239,7 +239,7 @@ appropriate transaction object for your conditions and actions. For
 example, if you had an action you wanted to perform based on the content
 of the last reply on stalled tickets, you could do something like:
 
-    /opt/rt5/bin/rt-crontool --search RT::Search::FromSQL \
+    /usr/bin/rt-crontool-5 --search RT::Search::FromSQL \
         --search-arg "Status = 'stalled' AND Queue = 'General'" \
         --action RT::Action::CheckLastCorrespond \
         --transaction last \
diff --git a/docs/customizing/assets/tutorial.pod b/docs/customizing/assets/tutorial.pod
index dee46ced..f3d32307 100644
--- a/docs/customizing/assets/tutorial.pod
+++ b/docs/customizing/assets/tutorial.pod
@@ -16,13 +16,13 @@ alt="Asset Search Results" /></p>
 You can follow along with the tutorial and try setting things up yourself to
 get a feel for all of the administrative controls. If you want to get a jump
 start, the files to set up this basic configuration are provided in the
-F<docs/customizing/assets> directory.
+F</usr/share/doc/request-tracker5/customizing/assets> directory.
 
 To load the test catalog, custom fields, and users, run the following from
 your RT directory:
 
-    sbin/rt-setup-database --action insert --datafile \
-      docs/customizing/assets/tutorialdata
+    rt-setup-database-5 --action insert --datafile \
+      /usr/share/doc/request-tracker5/customizing/assets/tutorialdata
 
 This will change the default catalog name, create some users, and give those
 users asset permissions. Only asset rights are granted, so you need to grant
diff --git a/docs/customizing/scrip_conditions_and_action.pod b/docs/customizing/scrip_conditions_and_action.pod
index 2e7de721..1bdd5cf3 100644
--- a/docs/customizing/scrip_conditions_and_action.pod
+++ b/docs/customizing/scrip_conditions_and_action.pod
@@ -52,7 +52,7 @@ the configuration at Admin > Global > Conditions and click on 'On Resolve'.
 The condition has a Name, which is displayed in the Condition dropdown when
 you create a scrip, and a Description to identify it. The Condition Module is
 the RT module that executes the condition, in this case C<StatusChange>. You
-can find the code in C</opt/rt5/lib/RT/Condition/StatusChange.pm> and view
+can find the code in C</usr/share/request-tracker5/lib/RT/Condition/StatusChange.pm> and view
 the documentation at L<RT::Condition::StatusChange>.
 
 Parameters to Pass shows the actual parameter that is passed to the module
@@ -82,7 +82,7 @@ Now assume we have an additional requirement to check if a custom field value
 we'll need to write some code. To start, create a new file for your new
 SpecialReviewNeeded module here:
 
-    /opt/rt5/local/lib/RT/Condition/SpecialReviewNeeded.pm
+    /usr/local/share/request-tracker5/lib/RT/Condition/SpecialReviewNeeded.pm
 
 Creating it in the C<local> directory will keep it safe when you apply
 RT upgrades in the future.
@@ -150,7 +150,7 @@ existing actions on the action display page at Admin > Global > Actions.
 You find Notify AdminCcs, which might be close. Taking
 a quick look you see it has a Name and Description, like conditions, and
 the module it calls is C<Notify>, which can be found at
-C</opt/rt5/lib/RT/Action/Notify.pm>.
+C</usr/share/request-tracker5/lib/RT/Action/Notify.pm>.
 
 The Parameter to Pass is AdminCc, and if you look at other notification
 actions you'll see many use Notify and just pass a different ticket role.
diff --git a/docs/customizing/search_result_columns.pod b/docs/customizing/search_result_columns.pod
index e38aef13..69a340e5 100644
--- a/docs/customizing/search_result_columns.pod
+++ b/docs/customizing/search_result_columns.pod
@@ -53,7 +53,7 @@ For our example, let's assume we want to display the number of messages
 First we need to determine where to put our callback. RT's core Column Map code
 for tickets is here:
 
-    share/html/Elements/RT__Ticket/ColumnMap
+    /usr/share/request-tracker5/html/Elements/RT__Ticket/ColumnMap
 
 We'll look there first, both to see some sample Column Maps and also to look
 for an appropriate callback to use to add our own. Looking in that file,
@@ -68,7 +68,7 @@ the callback we need.
 Following the callback documentation, we determine we can put our callback
 here:
 
-    local/html/Callbacks/MyRT/Elements/RT__Ticket/ColumnMap/Once
+    /usr/local/share/request-tracker5/html/Callbacks/MyRT/Elements/RT__Ticket/ColumnMap/Once
 
 where F<Once> is the name of the file where we'll put our code.
 
@@ -151,13 +151,13 @@ Now that we have our column map created, there is one more callback to add
 to make it available for all of our users in the Add Columns section in
 the Query Builder. This file builds the list of fields available:
 
-    share/html/Search/Elements/BuildFormatString
+    /usr/share/request-tracker5/html/Search/Elements/BuildFormatString
 
 Looking there, we see the default callback (the callback without an
 explicit C<CallbackName>) passes the C<@fields> array, so that will work.
 Create the file:
 
-    local/html/Callbacks/MyRT/Search/Elements/BuildFormatString/Default
+    /usr/local/share/request-tracker5/html/Callbacks/MyRT/Search/Elements/BuildFormatString/Default
 
 And put the following code in the F<Default> file:
 
diff --git a/docs/extending/external_custom_fields.pod b/docs/extending/external_custom_fields.pod
index bc9f5425..a72675f0 100644
--- a/docs/extending/external_custom_fields.pod
+++ b/docs/extending/external_custom_fields.pod
@@ -10,13 +10,13 @@ data from the external source.
 =head2 Introduction into writing source of values
 
 For each type of data source that you want, you'll need to put a file in
-F</opt/rt5/local/lib/RT/CustomFieldValues/> (or equivalent if you
-installed RT into someplace other than F</opt/rt5>). To get a sense of
+F</usr/local/request-tracker5/lib/RT/CustomFieldValues/>.
+To get a sense of
 the code that you'll need to write, take a look at the code in
-F</opt/rt5/lib/RT/CustomFieldValues/Groups.pm> for a simple example
+F</usr/share/request-tracker5/lib/RT/CustomFieldValues/Groups.pm> for a simple example
 which just uses RT's API to pull in a list of RT's groups.
 
-Running C<perldoc /opt/rt5/lib/RT/CustomFieldValues/External.pm> will
+Running C<perldoc /usr/share/request-tracker5/lib/RT/CustomFieldValues/External.pm> will
 show you the documentation for the API that needs to be fulfilled;
 copying and editing the C<Groups> example is probably a fine place to
 start.
diff --git a/docs/extensions.pod b/docs/extensions.pod
index 45c8b58b..c911e7e6 100644
--- a/docs/extensions.pod
+++ b/docs/extensions.pod
@@ -28,9 +28,8 @@ as follows:
 =item 1.
 
 B<Run C<perl Makefile.PL>> to check for dependencies, and minimum
-versions of RT.  If your RT is in an unusual location (not
-C</opt/rt5/>), you may need to set the C<RTHOME> environment variable to
-the root of your RT location.
+versions of RT. On Debian, Request Tracker knows where to find the
+various RT directories.
 
 =item 2.
 
@@ -60,7 +59,7 @@ C<Plugin()> command:
     Plugin( 'RT::Extension::Sample' );
 
 
-On RT 4.0, enabling them instead requires adding them to the C<@Plugins>
+On RT 5.0, enabling them instead requires adding them to the C<@Plugins>
 configuration:
 
     Set( @Plugins, 'RT::Extension::Example', 'RT::Extension::Sample' );
diff --git a/docs/full_text_indexing.pod b/docs/full_text_indexing.pod
index 4ed8d5f0..a00963c3 100644
--- a/docs/full_text_indexing.pod
+++ b/docs/full_text_indexing.pod
@@ -21,12 +21,12 @@ PostgreSQL 8.3 and above support full-text searching natively; to set up
 the required C<ts_vector> column, and create either a C<GiN> or C<GiST>
 index on it, run:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index
+    rt-setup-fulltext-index-5
 
 If you have a non-standard database administrator username or password,
 you may need to pass the C<--dba> or C<--dba-password> options:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index --dba postgres --dba-password secret
+    rt-setup-fulltext-index-5 --dba postgres --dba-password secret
 
 This will then tokenize and index all existing attachments in your
 database; it may take quite a while if your database already has a large
@@ -41,7 +41,7 @@ webserver after making these changes.
 
 To keep the index up-to-date, you will need to run:
 
-    /opt/rt5/sbin/rt-fulltext-indexer
+    rt-fulltext-indexer-5
 
 ...at regular intervals.  By default, this will only tokenize up to 200
 tickets at a time; you can adjust this upwards by passing C<--limit
@@ -53,7 +53,7 @@ You can suppress this message and other output to C<STDERR> using the
 C<--quiet> option; this is particularly useful when running the command via
 C<cron>:
 
-    /opt/rt5/sbin/rt-fulltext-indexer --quiet
+    rt-fulltext-indexer-5 --quiet
 
 =head1 MYSQL and MariaDB
 
@@ -71,12 +71,12 @@ index this content without creating an additional table.  To create
 the required table (which is InnoDB on versions of MySQL and MariaDB
 which support it), run:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index
+    rt-setup-fulltext-index-5
 
 If you have a non-standard database administrator username or password,
 you may need to pass the C<--dba> or C<--dba-password> options:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index --dba root --dba-password secret
+    rt-setup-fulltext-index-5 --dba root --dba-password secret
 
 This will then tokenize and index all existing attachments in your
 database; it may take quite a while if your database already has a large
@@ -91,7 +91,7 @@ webserver after making these changes.
 
 To keep the index up-to-date, you will need to run:
 
-    /opt/rt5/sbin/rt-fulltext-indexer
+    rt-fulltext-indexer-5
 
 ...at regular intervals.  By default, this will only tokenize up to 200
 tickets at a time; you can adjust this upwards by passing C<--limit
@@ -103,7 +103,7 @@ You can suppress this message and other output to C<STDERR> using the
 C<--quiet> option; this is particularly useful when running the command via
 C<cron>:
 
-    /opt/rt5/sbin/rt-fulltext-indexer --quiet
+    rt-fulltext-indexer-5 --quiet
 
 =head3 Caveats
 
@@ -149,12 +149,12 @@ L<http://sphinxsearch.com/docs/current.html#sphinxse-mysql51>.
 Once MySQL has been recompiled with SphinxSE, and Sphinx itself is
 installed, you may create the required SphinxSE communication table via:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index
+    rt-setup-fulltext-index-5
 
 If you have a non-standard database administrator username or password,
 you may need to pass the C<--dba> or C<--dba-password> options:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index --dba root --dba-password secret
+    rt-setup-fulltext-index-5 --dba root --dba-password secret
 
 This will also provide you with the appropriate C<%FullTextSearch>
 configuration to add to your F<RT_SiteConfig.pm>; you will need to
@@ -217,12 +217,12 @@ C<RT_SiteConfig.pm> must be updated.
 Oracle supports full-text indexing natively using the Oracle Text
 package.  Once Oracle Text is installed and configured, run:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index
+    rt-setup-fulltext-index-5
 
 If you have a non-standard database administrator username or password,
 you may need to pass the C<--dba> or C<--dba-password> options:
 
-    /opt/rt5/sbin/rt-setup-fulltext-index --dba sysdba --dba-password secret
+    rt-setup-fulltext-index-5 --dba sysdba --dba-password secret
 
 This will create an Oracle CONTEXT index on the Content column in the
 Attachments table, as well as several preferences, functions and
@@ -248,7 +248,7 @@ To get around it, you can grant it using C<sys> user instead, e.g.
 To update the index, you will need to run the following at regular
 intervals:
 
-    /opt/rt5/sbin/rt-fulltext-indexer
+    rt-fulltext-indexer-5
 
 This, in effect, simply runs:
 
@@ -259,7 +259,7 @@ This, in effect, simply runs:
 The amount of memory used for the sync can be controlled with the
 C<--memory> option:
 
-    /opt/rt5/sbin/rt-fulltext-indexer --memory 10M
+    rt-fulltext-indexer-5 --memory 10M
 
 If there is already an instance of C<rt-fulltext-indexer> running, new
 ones will exit with the message "rt-fulltext-indexer is already running".
@@ -267,7 +267,7 @@ You can suppress this message and other output to C<STDERR> using the
 C<--quiet> option; this is particularly useful when running the command via
 C<cron>:
 
-    /opt/rt5/sbin/rt-fulltext-indexer --quiet
+    rt-fulltext-indexer-5 --quiet
 
 Instead of being run via C<cron>, this may instead be run via a
 DBMS_JOB; read the B<Managing DML Operations for a CONTEXT Index>
diff --git a/docs/incremental-export/README b/docs/incremental-export/README
index 21c2c6f5..b0ac4d77 100644
--- a/docs/incremental-export/README
+++ b/docs/incremental-export/README
@@ -5,11 +5,13 @@ perform the following steps on your production server.
   it against your production RT instance. The output from --help will
   show you how to run it in check and then in fix mode.
 
-  Turn off all access to RT (this usually involves stopping your web and mail servers).
+  Turn off all access to RT (this usually involves stopping your web and mail
+    servers).
   Take a snapshot of the RT database.
-  Install Record_Local.pm into $RTHOME/lib/RT/.
+  Install Record_Local.pm into /usr/local/request-tracker5/lib/RT/.
   Create the IncrementalRecords table using the schema file for your database.
-  Add Set($IncrementalExport, 1); to your RT_SiteConfig.pm
+  Add Set($IncrementalExport, 1); to a file in
+    /etc/request-tracker5/RT_SiteConfig.d
   Bring your webserver back up.
   Confirm that changes in RT (such as a ticket status change or reply)
     result in records being added to IncrementalRecords.
@@ -22,8 +24,9 @@ until you import the incremental changes from production.
 For additional information on the following steps, please review
 rt-importer --help and rt-serializer --help
 
-During your final cutover, you will bring down the production web and mail servers for the final time.
-Run /opt/rt5/sbin/rt-serializer --incremental
+During your final cutover, you will bring down the production web and mail
+servers for the final time.
+Run rt-serializer --incremental
 This will create a directory named $Organization:date
 Once this has completed, you will copy this directory to the new server.
-On the new server, run /opt/rt5/sbin/rt-importer $Organization:date
+On the new server, run rt-importer-5 $Organization:date
diff --git a/docs/initialdata.pod b/docs/initialdata.pod
index c2d383a0..29e2b237 100644
--- a/docs/initialdata.pod
+++ b/docs/initialdata.pod
@@ -518,7 +518,7 @@ error checking and log any errors with C<< RT->Logger->error("...") >>!
 
 =head1 Running an initialdata file
 
-    /opt/rt5/sbin/rt-setup-database --action insert --datafile /path/to/your/initialdata
+    rt-setup-database-5 --action insert --datafile /path/to/your/initialdata
 
 This may prompt you for a database password.
 
diff --git a/docs/reminders.pod b/docs/reminders.pod
index da91a8a1..9faf33ce 100644
--- a/docs/reminders.pod
+++ b/docs/reminders.pod
@@ -41,7 +41,7 @@ C<rt-crontool> utility to schedule a job to send these emails for you.
 
 To schedule the reminders, add a line like the following to your RT crontab:
 
-    0 6 * * * root /opt/rt5/bin/rt-crontool \
+    0 6 * * * root /usr/bin/rt-crontool-5 \
                    --search RT::Search::FromSQL \
                    --search-arg 'Type = "reminder" and (Status = "open" or Status = "new")' \
                    --condition RT::Condition::BeforeDue \
diff --git a/docs/system_administration/database.pod b/docs/system_administration/database.pod
index 2de7666e..9d35a1a3 100644
--- a/docs/system_administration/database.pod
+++ b/docs/system_administration/database.pod
@@ -80,7 +80,7 @@ one no longer works or because you want to set up a dev machine to
 test on) you will need to create a fresh database and database user
 for RT to use.  RT can do that for you using:
 
-    /opt/rt5/sbin/rt-setup-database --action create,acl
+    rt-setup-database-5 --action create,acl
 
 By default, this will create an rt5 database and an rt_user user.  If
 you've specified a custom password in RT_SiteConfig.pm, RT will use
@@ -124,7 +124,7 @@ one no longer works or because you want to set up a dev machine to
 test on) you will need to create a fresh database and database user
 for RT to use.  RT can do part of that for you using:
 
-    /opt/rt5/sbin/rt-setup-database --action create
+    rt-setup-database-5 --action create
 
 You will need to create the rt_user separately.
 
@@ -150,8 +150,8 @@ your backups, or if you want to update a dev server using your backups,
 you will need to drop your database and recreate a fresh one to restore
 into.  RT can drop and recreate the database for you using:
 
-    /opt/rt5/sbin/rt-setup-database --action drop
-    /opt/rt5/sbin/rt-setup-database --action create
+    rt-setup-database-5 --action drop
+    rt-setup-database-5 --action create
 
 Remember that this will completely destroy the existing data and create
 a fresh database.  Your rt_user user will remain untouched.  Once this
@@ -321,7 +321,7 @@ these database types, you need to reset the sequences from 1, set in the newly
 created database, to the next available id for each table. The C<reset-sequences>
 script will reset these for you:
 
-    /opt/rt5/etc/upgrade/reset-sequences
+    /usr/share/request-tracker5/etc/upgrade/reset-sequences
 
 =item Test
 
diff --git a/docs/tracking-rt-configuration.pod b/docs/tracking-rt-configuration.pod
index cfc834b0..9798b0a1 100644
--- a/docs/tracking-rt-configuration.pod
+++ b/docs/tracking-rt-configuration.pod
@@ -25,14 +25,14 @@ the best results if they are in sync.
 
 A good first step before starting is to validate that your RT data is valid:
 
-    sbin/rt-validator --check
+    rt-validator-5 --check
 
-If L<rt-validator> reports any issues, it's good to investigate and fix those
+If L<rt-validator-5> reports any issues, it's good to investigate and fix those
 first.
 
 Next, take a JSON initialdata dump of our in dev RT:
 
-    sbin/rt-dump-initialdata --sync --directory base
+    rt-dump-initialdata-5 --sync --directory base
 
 This should create a new directory "base" that contains a file
 "initialdata.json" with your RT configuration. This contains
@@ -48,7 +48,7 @@ process. You can make these changes on the test RT and save. Once you
 are happy with the changes, run L<rt-dump-initialdata> again to output
 the changes:
 
-    sbin/rt-dump-initialdata --sync --directory new --base base/initialdata.json
+    rt-dump-initialdata-5 --sync --directory new --base base/initialdata.json
 
 For this run, C<--base> should point to the directory you created in the previous
 step.
@@ -151,7 +151,7 @@ haven't done so previously:
 
 You can now apply the changes.json initialdata to your production RT:
 
-    sbin/rt-setup-database --action insert --datafile new/changes.json
+    rt-setup-database-5 --action insert --datafile new/changes.json
 
 The output is JSON, so is human-readable. You can view the contents to
 confirm it contains what you expect before running against your
